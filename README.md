# derStandard.at / DER STANDARD 
WebApp für Ubuntu Touch

DER STANDARD wurde 1988 von Oscar Bronner als Tageszeitung für Wirtschaft, Politik und Kultur gegründet mit dem Anspruch, Qualitätsjournalismus in Österreich auf ein internationales Niveau zu bringen. Freiheit und Unabhängigkeit verkörpern daher wesentliche Eckpfeiler von Selbstverständnis und Positionierung des STANDARD als liberales Medium. Das erlaubt ihm, Leser und Leserinnen auf Augenhöhe zu begegnen und immer wieder als kritisches Medium zu Politik, Propaganda und Mainstream aufzutreten. Seit 30 Jahren.

